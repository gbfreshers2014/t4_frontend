'use strict';

var express = require('express'),
    libmojito = require('mojito'),
    cookieParser = require('cookie-parser'),
    //bodyParser = require('body-parser'),
    session = require('cookie-session'),
    methodOverride = require('method-override'),
    flash = require('connect-flash'),
    passport = require('./autoload/authentication.server.js').passport,
    app;

app = express();
app.set('port', process.env.PORT || 8666);
libmojito.extend(app, {
    context: {
        environment: "development"
    }
});
app.use(libmojito.middleware());



/*app.use(cookieParser());
app.use(express.bodyParser());
app.use(methodOverride());
app.use(express.session({ secret: 'gwn dog start' }));
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
// Initialize Passport!  Also use passport.session() middleware, to support
// persistent login sessions (recommended).
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());*/
//app.use(app.router);
//app.use(express.static(__dirname + '/../../public'));
app.use(express.cookieParser());
app.use(express.bodyParser());
//app.use(session({
//    secret: 'gwn dog start'
//}));
app.use(session({
    secret: 'gwn dog start'
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());



app.mojito.attachRoutes();

app.get('/', libmojito.dispatch('loginframe.index'));
//via passport
app.post('/', passport.authenticate('local', {
        failureRedirect: '/',
        failureFlash: "Wrong username or password"
    }),
    function(req, res) {
        //validation here

        if (req.user.role === 'admin') {
            res.redirect('/admin');
        } else if (req.user.role === 'employee') {
            res.redirect('/profile/' + req.user.uuid);
        } else {
            //error messages here

            res.end('Server error, please try again later');
        }

    });
app.get('/admin', passport.ensureAdmin, libmojito.dispatch('adminframe.index'));
app.get('/profile', passport.ensureAuthenticated, libmojito.dispatch('profileframe.index'));

app.get('/profile/:uuid/edit', passport.ensureAuthenticated, libmojito.dispatch('profileframe.edit'));
app.post('/profile/:uuid/edit', passport.ensureAuthenticated, libmojito.dispatch('profile.postedit'));
app.get('/profile/:uuid/delete', passport.ensureAdmin, libmojito.dispatch('profileframe.del'));

app.get('/profile/:uuid', passport.ensureAuthenticated, libmojito.dispatch('profileframe.index'));
app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});
app.get('/admin/leaves', passport.ensureAdmin, libmojito.dispatch('empLeavesFrame.index'));
app.get('/emp/leaves', passport.ensureAuthenticated, libmojito.dispatch('empLeavesFrame.index'));
app.get('/admin/add-emp', passport.ensureAdmin, libmojito.dispatch('addempframe.index'));
app.post('/admin/add-emp', passport.ensureAdmin, libmojito.dispatch('addempframe.addfn'));
app.get('/admin/manage-emp', passport.ensureAdmin, libmojito.dispatch('manageempframe.index'));
app.get('/admin/attendancetasks', passport.ensureAdmin, libmojito.dispatch('attendancetasksframe.index'));
app.get('/admin/attendancetasks/dateAttendance', passport.ensureAdmin, libmojito.dispatch('attendancetasksframe.dateAttendance'));
app.get('/admin/attendancetasks/dateAttendance/:uuid', passport.ensureAdmin, libmojito.dispatch('attendancetasksframe.dateAttendance'));
app.post('/admin/attendancetasks/dateAttendance', passport.ensureAdmin, libmojito.dispatch('attendancetasksframe.dateAttendance'));
app.post('/admin/attendancetasks/upload', passport.ensureAdmin, libmojito.dispatch('attendancetasksframe.upload'));
app.post('/admin/attendancetasks/upload/:uuid', passport.ensureAdmin, libmojito.dispatch('attendancetasksframe.uploadSingle'));
app.get('/search', passport.ensureAuthenticated, libmojito.dispatch('searchframe.result'));
app.post('/search', passport.ensureAuthenticated, libmojito.dispatch('searchframe.result'));
//app.get('/emp_profile',libmojito.dispatch('profile_emp_frame.index'));
app.get('/v1/emp-portal/uuid/leaves.json', passport.ensureAuthenticated, libmojito.dispatch('empLeavesFrame.index'));
app.get('/v1/emp-portal/uuid/checkleaves.json', passport.ensureAuthenticated, libmojito.dispatch('empLeaves.checkleaves'));


app.listen(app.get('port'), function() {
    console.log('Server listening on port ' + app.get('port') + ' ' +
        'in ' + app.get('env') + ' mode');
});

module.exports = app;