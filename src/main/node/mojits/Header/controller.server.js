/*global YUI, window, document*/
YUI.add('Header', function(Y, NAME) {
    'use strict';

    Y.namespace('mojito.controllers')[NAME] = {

        __call: function(ac) {
            //ac.assets.addCss('./css/index.css');

            ac.done({}, 'index');
        }
    };
}, '0.0.1', {
    requires: ['mojito', 'mojito-assets-addon', 'mojito-data-addon', 'mojito-models-addon', 'mojito-params-addon', 'mojito-http-addon']
});