/*global YUI, window, document*/
YUI.add('search-model', function(Y, NAME) {
    'use strict';
    var CONFIG = Y.gbee.config;
    var request = require('request');

    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },
        getData: function(callback) {
            callback(null, {
                some: 'data'
            });
        },
        searchResult: function(ac, callback) {
            var url = CONFIG.endpoints.empPortal + 'employees.json?param=' + ac.params.getFromBody('param');
            request({
                url: url,
                method: 'GET',
                json: true

            }, function(err, res, body) {
                callback(null, {
                    emplist: body.list
                });
            });

        }

    };

}, '0.0.1', {
    requires: ['mojito-rest-lib', 'mojito-params-addon', 'gbee-config']
});