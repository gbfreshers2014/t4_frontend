/*jslint anon:true, sloppy:true, nomen:true*/
/*global YUI, window, document*/
YUI.add('leaves', function(Y, NAME) {
    'use strict';
    /**
     * The leaves module.
     *
     * @module leaves
     */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */
        index: function(ac) {
            ac.models.get('model').getData(function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                ac.done({
                    PanelName: 'Admin Panel',
                    RoleName: ac._adapter.req.user.role,
                    class3: 'selected',
                    status: 'Mojito is working.',
                    data: data
                });
            });
        }

    };

}, '0.0.1', {
    requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon']
});