/*jslint anon:true, sloppy:true, nomen:true*/
/*global YUI, window, document*/
YUI.add('leaves-model', function(Y, NAME) {
    'use strict';
    /**
     * The leaves-model module.
     *
     * @module leaves
     */

    /**
     * Constructor for the LeavesModel class.
     *
     * @class LeavesModel
     * @constructor
     */
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, {
                some: 'data'
            });
        }

    };

}, '0.0.1', {
    requires: []
});