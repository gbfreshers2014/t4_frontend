/*global YUI, window, document*/
YUI.add('profile-model', function(Y, NAME) {
    "use strict";
    var CONFIG = Y.gbee.config;
    var request = require('request');

    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },
        getData: function(callback) {
            callback(null, { some: 'data' });
        },
        editData: function(data,uuidlink,callback) {
            var url = CONFIG.endpoints.empPortal + 'employees/' +uuidlink+ '.json';
            request({
                url: url,
                method: "PUT",
                body: JSON.stringify(data),
                json: true

            }, function(err,res, body){
                callback(err,body);
            });

        },
        profileData: function(uuidlink, callback){
            var url = CONFIG.endpoints.empPortal + 'employees/' +uuidlink+ '.json';
            request({
                url: url,
                method: "GET",
                body: {
                },
                json: true

            }, function(err,res, body){
                callback(null,body);
            });

        },
        deleteData: function(updatinguuid, uuid, callback) {
            var url = CONFIG.endpoints.empPortal + 'employees/' +uuid+ '.json' + '?updatingUuid='+updatinguuid;
            request({
                url: url,

                method: "DELETE",
                body: {
                },
                json: true

            }, function(err, res, body) {
                callback(null, body);
            });
        }

    };

}, '0.0.1', {requires: ['mojito-rest-lib','mojito-params-addon', 'gbee-config']});
