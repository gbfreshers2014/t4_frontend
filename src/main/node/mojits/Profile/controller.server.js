/*global YUI, window, document*/
YUI.add('profile', function(Y, NAME) {
    "use strict";
    Y.namespace('mojito.controllers')[NAME] = {
        postedit: function(ac){
            var jsonObj = ac.params.getFromBody('jsonObj');
            if(!(ac._adapter.req.user)) {
                ac.done("You are not an authenticated user");
                return;
            }
            jsonObj.updatingUuid = ac._adapter.req.user.uuid;
            var uuidlink;
            if(ac.params.route('uuid'))
            {
                uuidlink=ac.params.route('uuid');
            }
            else
            {
                uuidlink=ac._adapter.req.user.uuid;

            }

            ac.models.get('model').editData(jsonObj,uuidlink,function(err, res) {
                if (err) {
                    ac.error(err);
                    return;
                }

                var message;

                if(res.status.code === 0) {
                    message = "success";
                }
                else {
                    message = "failure";
                }
                ac.done({
                    message:message
                }, "json");

            });

        },
        index: function(ac) {
            ac.models.get('model').getData(function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                //ac.assets.addCss('./index.css');
            });
            var uuidlink;
            if(ac.params.route('uuid'))
            {
                uuidlink=ac.params.route('uuid');
            }
            else
            {
                uuidlink=ac._adapter.req.user.uuid;

            }

            //to check whether admin is making changes or employee
            var check_role={};

            if(ac._adapter.req.user.role==="admin") {
                check_role.admin='true';
            }
            else {
                check_role.admin=0;
            }

            // to check whether the employee is editing his own profile or trying to edit someone else profile
            if(uuidlink===ac._adapter.req.user.uuid) {
                check_role.allow_access='true';
            }
            else {
                check_role.allow_access='false';
            }

            function dateChanger(date) {
                var ar = date.split('-');
                if(ar.length === 3) {
                    date = ar[2] + '-' + ar[1] + '-' + ar[0];
                }
                return date;
            }

            ac.models.get('model').profileData(uuidlink,function(err, res) {
                var comm_list = [],
                    personal_details={},
                    address_list=[],
                    family_details=[], i;

                personal_details.fname= res.employeeDetails.personalDetails.firstName;
                personal_details.lname= res.employeeDetails.personalDetails.lastName;
                personal_details.emailid= res.employeeDetails.personalDetails.email;
                personal_details.empid= res.employeeDetails.personalDetails.employeeId;
                personal_details.bloodgrp=res.employeeDetails.personalDetails.bloodGroup;
                personal_details.empstatus=res.employeeDetails.personalDetails.employmentStatus;
                personal_details.dob=dateChanger(res.employeeDetails.personalDetails.dob);
                personal_details.doj=dateChanger(res.employeeDetails.personalDetails.doj);
                personal_details.designation= res.employeeDetails.personalDetails.designation;
                personal_details.uuidlink=uuidlink;

                for(i=0;i<res.employeeDetails.communicationDetails.length;i++)
                {
                    comm_list.push({
                        comm_type: res.employeeDetails.communicationDetails[i].type,
                        comm_details: res.employeeDetails.communicationDetails[i].details
                    });
                }

                var bool;
                for(i=0;i<res.employeeDetails.addressDetails.length;i++)
                {
                        if(i===0) {
                            bool=false;
                        }
                        else {
                            bool=true;
                        }

                        address_list.push({
                            sno: bool,
                            addr_type:res.employeeDetails.addressDetails[i].type,
                            street_address:res.employeeDetails.addressDetails[i].streetAddress,
                            city:res.employeeDetails.addressDetails[i].city,
                            state:res.employeeDetails.addressDetails[i].state,
                            country_code:res.employeeDetails.addressDetails[i].country,
                            zipcode:res.employeeDetails.addressDetails[i].zipcode

                        });
                }

                for (i = 0; i < res.employeeDetails.familyDetails.length; i++) {
                    if(i===0) {
                        bool=false;
                    }
                    else {
                        bool=true;
                    }

                    family_details.push({
                        sno: bool,
                        family_relation: res.employeeDetails.familyDetails[i].relation,
                        family_name: res.employeeDetails.familyDetails[i].fullName,
                        family_dependent: res.employeeDetails.familyDetails[i].dependent,
                        family_dob: dateChanger(res.employeeDetails.familyDetails[i].dob),
                        family_bloodGroup: res.employeeDetails.familyDetails[i].bloodGroup
                    });
                }

                ac.done({
                    PanelName: 'Profile Page',
                    RoleName: ac._adapter.req.user.role,
                    class2: 'selected',
                    uuidlink: uuidlink,
                    admin:check_role.admin,
                    personl:personal_details,
                    comm:comm_list,
                    addr:address_list,
                    family:family_details

                     });


            });



        },
        edit:function(ac) {
            var uuidlink;
            if(ac.params.route('uuid')) {
                uuidlink=ac.params.route('uuid');
            }
            else {
                uuidlink=ac._adapter.req.user.uuid;
            }

            //to check whether admin is making changes or employee
            var check_role={};
            if(ac._adapter.req.user.role === "admin") {
                check_role.admin=true;
            }
            else {
                check_role.admin=false;
            }

            // to check whether the employee is editing his own profile or trying to edit someone else profile
            if(uuidlink===ac._adapter.req.user.uuid) {
                check_role.allow_access='true';
            }
            else {
                check_role.allow_access='false';
            }

            ac.models.get('model').profileData(uuidlink,function(err, res) {

                var personal_details={},
                    comm_list = [],
                    address_list=[],
                    family_details=[],
                    role,
                    pass;

                personal_details.pass=res.loginCred.password;
                personal_details.role=res.loginCred.role;
                personal_details.fname= res.employeeDetails.personalDetails.firstName;
                personal_details.lname= res.employeeDetails.personalDetails.lastName;
                personal_details.emailid= res.employeeDetails.personalDetails.email;
                personal_details.empid= res.employeeDetails.personalDetails.employeeId;
                personal_details.bloodgrp=res.employeeDetails.personalDetails.bloodGroup;
                personal_details.empstatus=res.employeeDetails.personalDetails.employmentStatus;
                personal_details.dob=res.employeeDetails.personalDetails.dob;
                personal_details.doj=res.employeeDetails.personalDetails.doj;
                personal_details.designation= res.employeeDetails.personalDetails.designation;
                personal_details.uuidlink=uuidlink;


                for(var i=0;i<res.employeeDetails.communicationDetails.length;i++)
                {
                    comm_list.push({
                        comm_type: res.employeeDetails.communicationDetails[i].type,
                        comm_details: res.employeeDetails.communicationDetails[i].details
                    });
                }
                
                for(i=0;i<res.employeeDetails.addressDetails.length;i++)
                {

                        address_list.push({
                            sno: i+1,
                            addr_type:res.employeeDetails.addressDetails[i].type,
                            street_address:res.employeeDetails.addressDetails[i].streetAddress,
                            city:res.employeeDetails.addressDetails[i].city,
                            state:res.employeeDetails.addressDetails[i].state,
                            country_code:res.employeeDetails.addressDetails[i].country,
                            zipcode:res.employeeDetails.addressDetails[i].zipcode

                        });
                }

                for (i = 0; i < res.employeeDetails.familyDetails.length; i++) {

                    family_details.push({
                        family_relation: res.employeeDetails.familyDetails[i].relation,
                        family_name: res.employeeDetails.familyDetails[i].fullName,
                        family_dependent: res.employeeDetails.familyDetails[i].dependent,
                        family_dob: res.employeeDetails.familyDetails[i].dob,
                        family_bloodGroup: res.employeeDetails.familyDetails[i].bloodGroup,
                        family_dependentId: res.employeeDetails.familyDetails[i].dependentID
                    });
                }
                ac.done({
                    PanelName: 'Profile Edit Page',
                    RoleName: ac._adapter.req.user.role,
                    admin:check_role.admin,
                    allow_access:check_role.allow_access,
                    personl:personal_details,
                    pass:pass,
                    comm:comm_list,
                    addr:address_list,
                    family:family_details

                },"edit");


            });
        },
        del:function(ac) {
            if(!(ac._adapter.req.user) || (ac._adapter.req.user.role !== 'admin')) {
                ac.redirect('/');
                return;
            }
            var uuid;
            if(ac.params.route('uuid'))
            {
                uuid=ac.params.route('uuid');
            }
            ac.models.get('model').deleteData(ac._adapter.req.user.uuid, uuid, function(err, data) {
                if(err) {
                    ac.done({ mssg : "Internal Server error", admin:true}, "status");
                    return;
                }
                if(data && data.status && parseInt(data.status.code) === 0) {
                    ac.done({admin:true, mssg : "Deletion Successful",
                            uuid: uuid}, "status");
                    return;
                }
                ac.done({admin:true, mssg : "Deletion Unsuccessful"}, "status");
            });

        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon','mojito-params-addon', 'mojito-models-addon']});
