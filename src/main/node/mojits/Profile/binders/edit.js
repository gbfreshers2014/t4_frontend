/*global YUI, window, document, $*/
/*jshint multistr: true */
YUI.add('Profile-binder-edit', function(Y, NAME) {
    "use strict";
    Y.namespace('mojito.binders')[NAME] = {
        init: function (mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        bind: function (node) {
            var me = this;
            this.node = node;
            this.bindButtons(node, me);
            this.validation.addValidationEvent();
            var currdate = new Date();

            var dob = Y.one('#dobshow').get('value');
            var dobar = dob.split('-');
            if(dobar.length === 3) {
                Y.one('#dobshow').set('value', (dobar[2] + '-' + dobar[1] + '-' + dobar[0]));
            }
            var doj = Y.one('#dojshow').get('value');
            var dojar = doj.split('-');
            if(dojar.length === 3) {
                Y.one('#dojshow').set('value', (dojar[2] + '-' + dojar[1] + '-' + dojar[0]));
            }

            /*var calendar = new Y.Calendar({
                contentBox: '#mycalendarDob',
                width: '200px',
                showPrevMonth: true,
                showNextMonth: true,
                date: new Date(),
                //minimumDate: new Date(currdate.getFullYear(), '00', '01'),
                maximumDate: new Date()
            }).render();
            calendar.plug(Y.Plugin.CalendarNavigator)
            //console.log(calendar);
            //calendar.plug(Y.Plugin.Drag);
            //calendar.plug(Y.Plugin.Calendar.JumpNav, {
            //    yearStart: 1900,
            //    yearEnd: currdate.getFullYear(),
            //    closeAfterGo: false
            //});
            console.log('dob : ');
            console.log(calendar);*/
            /*var calendarDoj = new Y.Calendar({
                contentBox: '#mycalendarDoj',
                width: '200px',
                showPrevMonth: true,
                showNextMonth: true,
                date: new Date(),
                //minimumDate: new Date(currdate.getFullYear(), '00', '01'),
                maximumDate: new Date()
            }).render();

            calendarDoj.plug(Y.Plugin.Drag);
            calendarDoj.plug(Y.Plugin.Calendar.JumpNav, {
                yearStart: 1900,
                yearEnd: currdate.getFullYear(),
                closeAfterGo: false
            });

            var dtdate = Y.DataType.Date;

            // Listen to calendar's selectionChange event.
            /*calendar.on('selectionChange', function(ev) {

                // Get the date from the list of selected
                // dates returned with the event (since only
                // single selection is enabled by default,
                // we expect there to be only one date)
                var newDate = ev.newSelection[0];

                // Format the date and output it to a DOM
                // element.
                //Y.one("#datevalue").setHTML(dtdate.format(newDate));
                Y.one('#dob').set('value', dtdate.format(newDate));
                Y.one('#dobshow').set('value', dtdate.format(newDate, {
                    format: '%d-%m-%Y'
                }));
            });

            calendarDoj.on('selectionChange', function(ev) {

                // Get the date from the list of selected
                // dates returned with the event (since only
                // single selection is enabled by default,
                // we expect there to be only one date)
                var newDate = ev.newSelection[0];

                // Format the date and output it to a DOM
                // element.
                //Y.one("#datevalue").setHTML(dtdate.format(newDate));
                Y.one('#doj').set('value', dtdate.format(newDate));
                Y.one('#dojshow').set('value', dtdate.format(newDate, {
                    format: '%d-%m-%Y'
                }));
            });*/
        },
        validation: {
            addValidationEvent: function() {
                var personalDetailsValidator = Y.gbee.personalDetailsValidator;

                var firstNameNode = Y.one('#firstName'),
                    lastNameNode = Y.one('#lastName'),
                    emailIdNode = Y.one('#emailId'),
                    bloodGroupNode = Y.one('#bloodGroup'),
                    passwordNode = Y.one('#password'),
                    designationNode = Y.one('#designation');

                firstNameNode.on('change', function(e) {
                    var firstName = e.target.get('value');

                    if (!(personalDetailsValidator.nameValidator(firstName))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
                lastNameNode.on('change', function(e) {
                    var lastName = e.target.get('value');

                    if (!(personalDetailsValidator.nameValidator(lastName))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
                emailIdNode.on('change', function(e) {
                    var emailId = e.target.get('value');

                    if (!(personalDetailsValidator.emailValidator(emailId))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
                bloodGroupNode.on('change', function(e) {
                    var bloodGroup = e.target.get('value');

                    if (!(personalDetailsValidator.bloodGroupValidator(bloodGroup))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
                designationNode.on('change', function(e) {
                    var designation = e.target.get('value');

                    if (!(personalDetailsValidator.designationValidator(designation))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
                passwordNode.on('change', function(e) {
                    var password = e.target.get('value');

                    if (!(personalDetailsValidator.passwordValidator(password))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
            },

            validateFirstName: function () {
                if(Y.gbee.personalDetailsValidator.nameValidator(Y.one('#firstName').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#firstName').addClass('invalidField');
                    return false;
                }
            },

            validateLastName: function () {
                if(Y.gbee.personalDetailsValidator.nameValidator(Y.one('#lastName').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#lastName').addClass('invalidField');
                    return false;
                }
            },

            validateEmail: function () {
                if(Y.gbee.personalDetailsValidator.emailValidator(Y.one('#emailId').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#emailId').addClass('invalidField');
                    return false;
                }
            },

            validateBloodGroup: function () {
                if(Y.gbee.personalDetailsValidator.bloodGroupValidator(Y.one('#bloodGroup').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#bloodGroup').addClass('invalidField');
                    return false;
                }
            },

            validateDesignation: function () {
                if(Y.gbee.personalDetailsValidator.designationValidator(Y.one('#designation').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#designation').addClass('invalidField');
                    return false;
                }
            },

            validatePassword: function () {
                if(Y.gbee.personalDetailsValidator.passwordValidator(Y.one('#password').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#password').addClass('invalidField');
                    return false;
                }
            },

            validateDob: function () {
                if(Y.gbee.personalDetailsValidator.dobValidator(Y.one('#dob').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#dob').addClass('invalidField');
                    return false;
                }
            },

            validateDoj: function () {
                if(Y.gbee.personalDetailsValidator.dojValidator(Y.one('#doj').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#doj').addClass('invalidField');
                    return false;
                }
            },

            validateAll: function () {
                return (this.validateFirstName() &&
                    this.validateLastName() &&
                    this.validateEmail() &&
                    this.validateDesignation() &&
                    this.validateBloodGroup() &&
                    this.validatePassword());// &&
                //this.validateDob() &&
                //this.validateDoj());
            }
        },

        bindButtons: function (node, me) {
            Y.one("#uuid").setAttribute("readonly","readonly");
            Y.one("#emailId").setAttribute("readonly","readonly");
            Y.one("#empId").setAttribute("readonly","readonly");
            var role_dropdown = Y.one('#role').all('option');
            role_dropdown.each(function (option) {
                if (option.get('text').toLowerCase() === Y.one('#getRole').get('value')) {
                    option.setAttribute('selected', 'selected');
                }
            });
            var empStatus_dropdown = Y.one('#empStatus').all('option');
            empStatus_dropdown.each(function (option) {
                if (option.get('text').toLowerCase() === Y.one('#getEmpStatus').get('value')) {
                    option.setAttribute('selected', 'selected');
                }
            });
            var family_loop= 0,addr_loop= 0,comm_loop=0;

            var comm_template='<div id="commDiv"> \
            <div class="form_row">\
            <label>Communication Type:</label>\
            <select class="form_select" id="comm_type">\
            <option value="0">email</option><option value="1">skype</option><option value="2">phone-primary</option><option value="3">phone-office</option><option value="4">phone-other</option><option value="5">twitter</option><option value="6">facebook</option><option value="7">linkedin</option>\
            </select>\
            </div><div class="form_row">\
            <label>Details:</label>\
            <input type="text" class="form_input" id="comm_details"  />\
            <input type="submit" class="form_submit" value="Remove This Communication Detail" id="remove_comm_button" />\
            </div></div>';

            var address_template = '<div id="addrDiv"> \
            <div class="form_row"> \
            <label>Address Type:</label> \
            <select class="form_select" id="addr_type"> \
              <option value="0">permanent</option><option value="1">present</option><option value="2">other</option> \
            </select> \
            </div><div class="form_row">\
            <label>Street Address:</label>\
            <input type="text" class="form_input" id="street_address"> \
            </div><div class="form_row">\
            <label>City:</label>\
            <input type="text" class="form_input2" id="city">\
            <label>State:</label>\
            <input type="text" class="form_input2" id="state">\
            </div><div class="form_row">\
            <label>Country Code:</label>\
            <input type="text" class="form_input2" id="country_code">\
            <label>Zip Code:</label>\
            <input type="text" class="form_input2" id="zipcode">\
            <input type="submit" class="form_submit" value="Remove This Address Details" id="remove_address_button">\
            </div></div>';

            var family_template = '<div id="familyDiv"> \
                <div class="form_row"> \
                <label>Relation Name:</label> \
                <select class="form_select" id="family_type"> <option value="0">father</option> <option value="1">mother</option> <option value="2">spouse</option> <option value="3">child</option>  \
                </select> \
                </div><div class="form_row"> \
                <label>Full Name:</label> \
                <input type="text" class="form_input2" id="family_fullName"  /> \
                <label>Dependent:</label> \
                <select class="form_select" id="family_dependent"> <option value="0">true</option> <option value="1">false</option> \
                </select>\
                </div><div class="form_row"> \
                <label>Date of birth:</label> \
                <input type="text" class="form_input2 datepickerdob" readonly/> \
                <label>Blood Group:</label> \
                <input type="text" class="form_input2" id="family_bloodGroup"  /> \
                </div>\
                <div class="form_row"> \
                <input type="submit" class="form_submit" value="Remove This Family Member Details" id="remove_family_button" /> \
                </div>\
                </div>';

            var addressList = Y.one('#address_block').all('#addrDiv');
            addressList.each(function (address) {
                var addr_type = address.one('#addr_type_get');
                if(!(addr_type)) {
                    return;
                }
                var addr_dropdown = address.one('#addr_type').all('option');
                addr_dropdown.each(function (option) {
                    if (option.get('text').toLowerCase() === addr_type.get('value')) {
                        option.setAttribute('selected', 'selected');
                    }
                });
            });
            
            var commList = Y.one('#comm_block').all('#commDiv');
            commList.each(function (comm) {
                var comm_type = comm.one('#comm_type_get');
                if(!(comm_type)) {
                    return;
                }
                var comm_dropdown = comm.one('#comm_type').all('option');
                comm_dropdown.each(function (option) {
                    if (option.get('text').toLowerCase() === comm_type.get('value')) {
                        option.setAttribute('selected', 'selected');
                    }
                });
            });

            var familyList = Y.one('#family_block').all('#familyDiv');
            familyList.each(function (family) {
                var family_type = family.one('#family_type_get');
                if(!(family_type)) {
                    return;
                }
                var family_dropdown = family.one('#family_type').all('option');
                family_dropdown.each(function (option) {
                    if (option.get('text').toLowerCase() === family_type.get('value')) {
                        option.setAttribute('selected', 'selected');
                    }
                });
            });
            familyList.each(function (family) {
                var family_dependent = family.one('#family_dependent_get');
                if(!(family_dependent)) {
                    return;
                }
                var family_dep_dropdown = family.one('#family_dependent').all('option');
                family_dep_dropdown.each(function (option) {
                    if (option.get('text').toLowerCase() === family_dependent.get('value')) {
                        option.setAttribute('selected', 'selected');
                    }
                });

                var family_bloodGroup = family.one('#family_bloodGroup');
                family_bloodGroup.on('change', function (e) {
                    if(!(Y.gbee.personalDetailsValidator.bloodGroupValidator(e.target.get('value')))) {
                        e.target.addClass('invalidField');
                    }
                    else {
                        e.target.removeClass('invalidField');
                    }
                });

                var family_name = family.one('#family_fullName');
                family_name.on('change', function (e) {
                    if(!(Y.gbee.personalDetailsValidator.fullNameValidator(e.target.get('value')))) {
                        e.target.addClass('invalidField');
                    }
                    else {
                        e.target.removeClass('invalidField');
                    }
                });
            });
            var len = familyList._nodes.length;
            //familyList.each(function (family) {
            var family_count = 0;
            for(var i=0; i<len; i++) {
                var family = familyList.item(i);
                family_count++;
                family.one('.datepickerdob').set('id', 'family_dob'+family_count);
                var ar = family.one('.datepickerdob').get('value').split('-');
                if(ar.length === 3) {
                    family.one('.datepickerdob').set('value', ar[2] + '-' + ar[1] + '-' + ar[0]);
                }
            }

            var family_bljq = $( '#family_block' );
            /*$('#family_block').delegate('#family_dob', "",function() {
                $(this).datepicker( {changeYear:true,
                    changeMonth: true,
                    yearRange: "1900:c",
                    dateFormat: "dd-mm-yy"
                });
            });*/

            /*family_bljq.children().each(function (n, i) {
                $(this).find('#family_dob').datepicker( {
                    changeYear:true,
                    changeMonth: true,
                    yearRange: "1900:c",
                    dateFormat: "dd-mm-yy"
                });
            });*/
            $(".datepickerdob").datepicker({
                changeYear:true,
                changeMonth: true,
                yearRange: "1900:c",
                dateFormat: "dd-mm-yy"
            });
            Y.one('#address_button').on('click', function(e) {

                addr_loop++;
                Y.one('#address_block').appendChild(address_template);
            });
            Y.one('#address_block').delegate('click', function () {
                this.ancestor('#addrDiv').remove();
                addr_loop--;
            }, '#remove_address_button');

            Y.one('#comm_button').on('click', function(e) {

                comm_loop++;
                Y.one('#add_comm_details_here').appendChild(comm_template);

            });
            Y.one('#comm_block').delegate('click', function () {
                this.ancestor('#commDiv').remove();
                comm_loop--;
            }, '#remove_comm_button');


            Y.one('#family_button').on('click', function(e) {

                family_loop++;
                var family = Y.Node.create(family_template);
                //var remove_bttn = family.one('#remove_family_button');
                //var calendar = Y.Node.create(   '<div class="yui3-skin-sam" id="mycalendarDobdiv">' +
                //                                    '<div id="mycalendarDob' + family_loop + '"></div>' +
                //                                '</div>');
                //family.insert(calendar, remove_bttn);
                family_count++;
                family.one('.datepickerdob').set('id', 'family_dob'+family_count);

                Y.one('#add_family_details_here').appendChild(family);

                $('.datepickerdob').datepicker({
                    changeYear:true,
                    changeMonth: true,
                    yearRange: "1900:c",
                    dateFormat: "dd-mm-yy"
                });

                /*var calendardiv = family.one('#mycalendarDob');
                var calendarDobF = new Y.Calendar({
                    contentBox: calendardiv,
                    width: '200px',
                    showPrevMonth: true,
                    showNextMonth: true,
                    date: new Date(),
                    //minimumDate: new Date(currdate.getFullYear(), '00', '01'),
                    maximumDate: new Date()
                });

                //console.log(calendarDobF);
                var currdate = new Date();
                //calendarDobF.plug(Y.Plugin.Drag);
                calendarDobF.plug(Y.Plugin.CalendarNavigator);
                console.log(Y.Plugin.CalendarNavigator);
                //calendarDobF.plug(Y.Plugin.Calendar.JumpNav, {
                //    yearStart: 1900,
                //    yearEnd: currdate.getFullYear(),
                //});
                var ss = Y.Plugin.Calendar.JumpNav;
                console.log(ss);
                console.log('plugin attached');
                calendarDobF.render();
                var dtdate = Y.DataType.Date;

            // Listen to calendar's selectionChange event.
                calendarDobF.on('selectionChange', function(ev) {

                    // Get the date from the list of selected
                    // dates returned with the event (since only
                    // single selection is enabled by default,
                    // we expect there to be only one date)
                    var newDate = ev.newSelection[0];

                    // Format the date and output it to a DOM
                    // element.
                    //Y.one("#datevalue").setHTML(dtdate.format(newDate));
                    family.one('#family_dob').set('value', dtdate.format(newDate));
                    //console.log(Y.one('#family_dob').get('value'));
                    family.one('#family_dob_show').set('value', dtdate.format(newDate, {
                        format: '%d-%m-%Y'
                    }));
                });*/
            });
            Y.one('#family_block').delegate('click', function () {
                this.ancestor('#familyDiv').remove();
                family_loop--;
            }, '#remove_family_button');

            if (!(Y.one('#RoleCheck').get('value')))
                {
                    if (Y.one('#EmpCheck').get('value')==='false')
                    {
                        Y.config.win.location="/profile";
                    }
                    else
                    {
                        Y.one("#firstName").setAttribute("readonly","readonly");
                        Y.one("#lastName").setAttribute("readonly","readonly");
                        //Y.one("#dobshow").setAttribute("readonly","readonly");
                        $('#dobshow').datepicker("option", "disabled", true);
                        $('#dojshow').datepicker("option", "disabled", true);
                        //Y.one("#dojshow").setAttribute("readonly","readonly");
                        Y.one("#designation").setAttribute("readonly","readonly");
                        Y.one("#role").setAttribute("disabled","true");
                        Y.one("#empStatus").setAttribute("disabled","true");
                    }
                }
                else {
                    $( "#dobshow" ).datepicker({
                        changeYear:true,
                        changeMonth: true,
                        yearRange: "1900:c",
                        dateFormat: "dd-mm-yy"
                    });

                    $( "#dojshow" ).datepicker({
                        changeYear:true,
                        changeMonth: true,
                        yearRange: "1900:c",
                        dateFormat: "dd-mm-yy"
                    });
                }

            Y.one('#submit_button').on("click", function verification(e) {
                e.preventDefault();
                var err= 0,lp;
                var invalidf = Y.one('.invalidField');
                if((!(me.validation.validateAll())) || invalidf) {
                    window.alert('Please correct the fields marked as Red');
                    return;
                }

                if (Y.one('#bloodGroup').get('value').length < 2) {
                    err = 1;
                    Y.one('#bloodGroup').addClass("missing_class");
                }


                if (err === 0) {
                    var first_name = Y.one('#firstName').get('value'),
                        last_name = Y.one('#lastName').get('value'),
                        email_id = Y.one('#emailId').get('value'),
                        emp_id = Y.one('#empId').get('value'),
                        uuid = Y.one('#uuid').get('value'),
                        pass=Y.one('#password').get('value'),
                        designation = Y.one('#designation').get('value'),
                        blood_group = Y.one('#bloodGroup').get('value');
                        var dob = Y.one('#dobshow').get('value');
                        var dobar = dob.split('-');
                        if(dobar.length === 3) {
                            dob = dobar[2] + '-' + dobar[1] + '-' + dobar[0];
                        }
                        var doj = Y.one('#dojshow').get('value');
                        var dojar = doj.split('-');
                        if(dojar.length === 3) {
                            doj = dojar[2] + '-' + dojar[1] + '-' + dojar[0];
                        }

                    
                    var role_value=Y.one('#role').get('value');
                    var emp_value=Y.one('#empStatus').get('value');

                   var role= Y.one('#role').get('options').item(role_value).get('text');
                   var emp_status=Y.one('#empStatus').get('options').item(emp_value).get('text');

                    
                    var jsonObj = {};
                    jsonObj.uuid=uuid;
                    jsonObj.employeeDetails = {};
                    jsonObj.loginCred = {
                        role: role,
                        password: pass
                    };
                    jsonObj.employeeDetails.familyDetails = [];
                    jsonObj.employeeDetails.addressDetails = [];
                    jsonObj.employeeDetails.communicationDetails = [];

                    jsonObj.employeeDetails.personalDetails = {
                        firstName: first_name,
                        lastName: last_name,
                        email: email_id,
                        employeeId: emp_id,
                        designation: designation,
                        employmentStatus:emp_status,
                        bloodGroup: blood_group,
                        dob: dob,
                        doj: doj
                    };

                    var family_details = Y.one('#family_block'),
                        memberlist = family_details.all('#familyDiv'),
                        member, v1, v2;

                    for(var i=0; i<memberlist._nodes.length; i++) {
                        member = memberlist.item(i);
                        v1 = member.one('#family_type').get('value');
                        v2 = member.one('#family_dependent').get('value');
                        var relation_name = member.one('#family_type').get('options').item(v1).get('text');
                        var dependent = member.one('#family_dependent').get('options').item(v2).get('text');
                        var dobb = member.one('.datepickerdob').get('value');
                        var dobars = dobb.split('-');
                        if(dobars.length === 3) {
                            dobb = dobars[2] + '-' + dobars[1] + '-' + dobars[0];
                        }
                        var bloodgrp = member.one('#family_bloodGroup').get('value');
                        var dependentId = null;
                        if(member.one('#family_dependentId'))
                        {
                            dependentId=member.one('#family_dependentId').get('value');
                        }
                        var fullname= member.one('#family_fullName').get('value');
                        var family_obj;
                        if(dependentId) {
                            family_obj =
                            {
                                relation: relation_name ,
                                fullName: fullname,
                                dependent: dependent ,
                                bloodGroup: bloodgrp,
                                dob: dobb,
                                dependentID: dependentId
                            };
                        }
                        else {
                            family_obj =
                            {
                                relation: relation_name ,
                                fullName: fullname,
                                dependent: dependent ,
                                bloodGroup: bloodgrp,
                                dob: dobb,
                            };
                        }
                        jsonObj.employeeDetails.familyDetails.push(family_obj);
                    }

                    var addr_detail=Y.one('#address_block');
                    memberlist=addr_detail.all('#addrDiv');
                    for(i=0; i<memberlist._nodes.length; i++) {
                        member = memberlist.item(i);
                        v1 = member.one('#addr_type').get('value');
                        var addr_type = member.one('#addr_type').get('options').item(v1).get('text');
                        var streetaddress = member.one('#street_address').get('value');
                        var city = member.one('#city').get('value');
                        var state = member.one('#state').get('value');
                        var zipcode = member.one('#zipcode').get('value');
                        var countrycode= member.one('#country_code').get('value');
                        var address_obj =
                        {
                            type: addr_type ,
                            streetAddress: streetaddress,
                            city: city,
                            state: state,
                            country: countrycode,
                            zipcode: zipcode
                        };
                        jsonObj.employeeDetails.addressDetails.push(address_obj);
                    }

                    var comm_detail=Y.one('#comm_block');
                    memberlist=comm_detail.all('#commDiv');
                    for(i=0; i<memberlist._nodes.length; i++) {
                        member = memberlist.item(i);
                        v1 = member.one('#comm_type').get('value');
                        var comm_type = member.one('#comm_type').get('options').item(v1).get('text');
                        var details = member.one('#comm_details').get('value');
                        var comm_obj =
                        {
                            type: comm_type ,
                            details: details
                        };
                        jsonObj.employeeDetails.communicationDetails.push(comm_obj);
                        
                    }

                    Y.io("/profile/"+uuid+"/edit", {
                        sync: true,
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        data: JSON.stringify({jsonObj:jsonObj}),
                        on: {
                            complete: function (id, o, args) {
                                var response = o.responseText;
                                response = JSON.parse(response);
                                if (response.message === "success") {
                                    window.alert('Update successful');
                                    Y.config.win.location ='/profile/'+uuid;
                                }
                                else  {
                                    window.alert('Update failed');
                                }
                            }
                        }
                    });

                }

            });

        }

    };
}, '0.0.1', {requires: ['event-mouseenter', 'mojito-client', 'event-click','calendar', 'gallery-calendar-jumpnav', 'gbee-validator']});
