/*global YUI, window, document*/
YUI.add('login-model', function(Y, NAME) {
    'use strict';
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },
        getData: function(callback) {
            callback(null, {
                some: 'data'
            });
        }
    };

}, '0.0.1', {
    requires: ['mojito-rest-lib', 'mojito-params-addon']
});