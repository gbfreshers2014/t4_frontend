/*global YUI, window, document*/
YUI.add('login', function(Y, NAME) {
    'use strict';
    Y.namespace('mojito.controllers')[NAME] = {

        index: function(ac) {
            ac.assets.addCss('./css/index.css');
            ac.done({}, 'login');
        }
    };
}, '0.0.1', {
    requires: ['mojito', 'mojito-assets-addon', 'mojito-data-addon', 'mojito-models-addon', 'mojito-params-addon', 'mojito-http-addon']
});