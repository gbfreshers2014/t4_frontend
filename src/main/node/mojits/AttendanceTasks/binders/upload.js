/*jslint anon:true, sloppy:true, nomen:true*/
/*global YUI, window, document*/
YUI.add('attendancetasks-binder-upload', function(Y, NAME) {
  'use strict';
  /**
   * The attendancetasks-binder-index module.
   *
   * @module attendancetasks-binder-index
   */

  /**
   * Constructor for the AttendanceTasksBinderIndex class.
   *
   * @class AttendanceTasksBinderIndex
   * @constructor
   */
  Y.namespace('mojito.binders')[NAME] = {

    /**
     * Binder initialization method, invoked after all binders on the page
     * have been constructed.
     */
    init: function(mojitProxy) {
      this.mojitProxy = mojitProxy;
    },

    /**
     * The binder method, invoked to allow the mojit to attach DOM event
     * handlers.
     *
     * @param node {Node} The DOM node to which this mojit is attached.
     */
    bind: function(node) {
      var me = this;
      this.node = node;
      var mp = this.mojitProxy;

      var file = node.one('#fileupload');
      var form = node.one('#uploadform');
      var inp = node.all('input[name=attendanceAction]');
      var single_employee = mp.data.get('single_employee');
      if (!single_employee) {
        inp.on('change', function(e) {
          if (e.target.get('value') === 'uploaded') {
            file._node.required = true;
            form.set('enctype', 'multipart/form-data');
          } else {
            file._node.required = false;
            form.set('enctype', 'application/x-www-form-urlencoded');
          }
        });
      }
      var currdate = new Date();

      var calendar = new Y.Calendar({
        contentBox: '#mycalendar',
        width: '340px',
        showPrevMonth: true,
        showNextMonth: true,
        date: new Date(),
        minimumDate: new Date(currdate.getFullYear(), '00', '01'),
        maximumDate: new Date()
      }).render();

      calendar.plug(Y.Plugin.Drag);
      calendar.plug(Y.Plugin.Calendar.JumpNav, {
        yearStart: 1900,
        yearEnd: currdate.getFullYear(),
        closeAfterGo: false
      });
      // Get a reference to Y.DataType.Date
      var dtdate = Y.DataType.Date;

      // Listen to calendar's selectionChange event.
      calendar.on('selectionChange', function(ev) {

        // Get the date from the list of selected
        // dates returned with the event (since only
        // single selection is enabled by default,
        // we expect there to be only one date)
        var newDate = ev.newSelection[0];

        // Format the date and output it to a DOM
        // element.
        //Y.one("#datevalue").setHTML(dtdate.format(newDate));
        Y.one('#datevalue').set('value', dtdate.format(newDate));
        Y.one('#dateshow').setHTML(dtdate.format(newDate, {format: '%d-%m-%Y'}));
      });
    }

  };

}, '0.0.1', {
  requires: ['event-mouseenter', 'mojito-client', 'calendar', 'attribute', 'mojito-data-addon', 'calendar-navigator', 'gallery-calendar-jumpnav']
});