/*jslint anon:true, sloppy:true, nomen:true*/
/*global YUI, window, document*/
YUI.add('attendancetasks-model', function(Y, NAME) {
    'use strict';
    var CONFIG = Y.gbee.config;
    /**
     * The attendancetasks-model module.
     *
     * @module attendancetasks
     */

    /**
     * Constructor for the AttendanceTasksModel class.
     *
     * @class AttendanceTasksModel
     * @constructor
     */

    var request = require('request');
    var fs = require('fs');

    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, {
                some: 'data'
            });
        },

        uploadAttendance: function(updadtingUuid, date, attaction, filePath, callback) {
            var url = CONFIG.endpoints.empPortal + 'leaves.json';
            // try it with standard post request when the api is working
            var r = request.post(url, function(err, res, body) {
                if (err) {
                    callback(err);
                    return;
                }
                if (filePath) {
                    fs.unlink(filePath, function(err) {
                        if (err) {
                            return;
                        }
                    });
                }
                callback(null, JSON.parse(body));
            });
            var form = r.form();
            form.append('date', date);
            form.append('action', attaction);
            form.append('uploadingUuid', updadtingUuid);
            if (attaction === 'uploaded') {
                form.append('file', fs.createReadStream(filePath));
            }
        },

        uploadAttendanceSingle: function(updadtingUuid, uuid, date, attaction, callback) {
            var reqObj = {
                updatingUuid: updadtingUuid,
                date: date,
                attendanceStatus: attaction
            };
            var url = CONFIG.endpoints.empPortal + 'employees/' + uuid + '/leaves.json';
            request({
                url: url,
                headers: {
                    'Content-Type': 'application/json'
                },
                method: 'POST',
                body: JSON.stringify(reqObj)

            }, function(err, res, body) {
                if (err) {
                    callback(err);
                    return;
                }
                callback(null, body);
            });
        }

    };

}, '0.0.1', {
    requires: ['gbee-config']
});