/*jslint anon:true, sloppy:true, nomen:true*/
/*global YUI, window, document*/
YUI.add('attendancetasks', function(Y, NAME) {
  'use strict';
  /**
   * The attendancetasks module.
   *
   * @module attendancetasks
   */

  /**
   * Constructor for the Controller class.
   *
   * @class Controller
   * @constructor
   */
  var fs = require('fs');
  Y.namespace('mojito.controllers')[NAME] = {

    /**
     * Method corresponding to the 'index' action.
     *
     * @param ac {Object} The ActionContext that provides access
     *        to the Mojito API.
     */

    index: function(ac) {
      if (!(ac._adapter.req.user)) {
        ac.redirect('/');
      }
      //ac.assets.addCss('./index.css');
      ac.models.get('model').getData(function(err, data) {
        if (err) {
          ac.error(err);
          return;
        }
        ac.done({
          PanelName: 'Admin Panel',
          RoleName: ac._adapter.req.user.role,
          admin: true
        });
      });
    },

    dateAttendance: function(ac) {
      if (!(ac._adapter.req.user)) {
        ac.redirect('/');
      }
      var singleEmployeeAttendance = false;
      var uuid = ac.params.route('uuid');
      if (uuid) {
        singleEmployeeAttendance = true;
      }
      //ac.assets.addCss('./index.css');
      var date = ac.params.getFromUrl('date') || ac.params.getFromBody('date');
      var date_el, date_show;
      if(date) {
        date_el = date.split('-');
        date_show = date_el[2] + '-' + date_el[1] + '-' + date_el[0];
      }
      ac.data.setAttrs({
        date: ac.params.getFromBody('date'),
        single_employee: singleEmployeeAttendance
      }, 'upload');
      ac.done({
        PanelName: 'Admin Panel',
        RoleName: ac._adapter.req.user.role,
        admin: true,
        single_employee: singleEmployeeAttendance,
        uuid: uuid,
        date: date,
        date_show: date_show
      }, 'upload');
    },

    upload: function(ac) {
      if (!(ac._adapter.req.user)) {
        ac.redirect('/');
      }
      var req = ac.http.getRequest();
      var files = req.files;
      var action = ac.params.getFromBody('attendanceAction');
      var date = ac.params.getFromBody('date');
      var file = files && files.csvFile;

      if (!date || !action || (action !== 'processed' && action !== 'holiday' && action !== 'uploaded')) {
        ac.done({
          mssg: 'Malformed request 1',
          admin: true
        }, 'uploadStatus');
        return;
      }

      if (action === 'uploaded') {
        if (!file) {
          ac.done({
            mssg: 'Malformed request 2',
            admin: true
          }, 'uploadStatus');
          return;
        }
      } else {
        if (file) {
          fs.unlink(file.path, function(err) {
          });
        }
      }

      ac.models.get('model').uploadAttendance(ac._adapter.req.user.uuid, date, action, (file && file.path), function(err, res) {
        if (err) {
          ac.done({
            PanelName: 'Admin Panel',
            mssg: 'Internal Server Error',
            admin: true
          }, 'uploadStatus');
        } else {
          if (!(res.status) || !(res.status.hasOwnProperty('code')) || (res.status.code !== 0)) {
            ac.done({
              PanelName: 'Admin Panel',
              RoleName: ac._adapter.req.user.role,
              mssg: 'Upload Unsuccessful',
              admin: true
            }, 'uploadStatus');
          } else {
            ac.done({
              PanelName: 'Admin Panel',
              RoleName: ac._adapter.req.user.role,
              mssg: 'Upload Successful',
              admin: true
            }, 'uploadStatus');
          }
        }
      });
    },

    uploadSingle: function(ac) {
      if (!(ac._adapter.req.user) || (ac._adapter.req.user.role !== 'admin')) {
        ac.done();
        return;
      }
      var action = ac.params.getFromBody('attendanceAction');
      var date = ac.params.getFromBody('date');
      var uuid = ac.params.route('uuid');

      if (!date || !action || (action !== 'present' && action !== 'holiday' && action !== 'on-leave' && action !== 'absent')) {
        ac.done({
          PanelName: 'Admin Panel',
          mssg: 'Malformed request 1',
          admin: true
        }, 'uploadStatus');
        return;
      }


      ac.models.get('model').uploadAttendanceSingle(ac._adapter.req.user.uuid, uuid, date, action, function(err, res) {
        var message, reason;
        if (err) {
          message = 'Internal Server Error';
        } else {
          res = JSON.parse(res);
          if (res && res.status) {
            if (parseInt(res.status.code) === 0) {
              message = 'Update Successful';
            } else {
              message = 'Update Unsuccessful';
              if (parseInt(res.status.code) === 1) {
                reason = 'API could not update for some unknown reason';
              }
              if (parseInt(res.status.code) === 2) {
                reason = 'There is no previous entry for this date for this particular employee, so update is not possible';
              }
            }
          } else {
            message = 'Malformed response from API';
          }
        }
        ac.done({
          PanelName: 'Admin Panel',
          mssg: message,
          reason: reason,
          admin: true
        }, 'uploadStatus');
      });
    }

  };

}, '0.0.1', {
  requires: ['mojito',
    'mojito-assets-addon',
    'mojito-models-addon',
    'mojito-params-addon',
    'mojito-http-addon',
    'fs',
    'mojito-data-addon',
    'cssfonts'
  ]
});