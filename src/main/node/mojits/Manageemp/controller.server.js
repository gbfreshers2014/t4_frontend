/*jslint anon:true, sloppy:true, nomen:true*/
/*global YUI, window, document*/
YUI.add('manageemp', function(Y, NAME) {
    'use strict';
    /**
     * The manageemp module.
     *
     * @module manageemp
     */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */
        index: function(ac) {
            if (!(ac._adapter.req.user) || (ac._adapter.req.user.role !== 'admin')) {
                ac.redirect('/');
                return;
            }
            ac.models.get('model').getData(function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                var emplist = [];
                if (data && data.list) {
                    emplist = data.list;
                    for (var i = 0; i < emplist.length; i++) {
                        emplist[i].admin = true;
                        emplist[i].sno = i + 1;
                    }
                }
                ac.done({
                    PanelName: 'Admin Panel',
                    RoleName: ac._adapter.req.user.role,
                    emplist: emplist,
                    admin: true
                });
            });
        }

    };

}, '0.0.1', {
    requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon']
});