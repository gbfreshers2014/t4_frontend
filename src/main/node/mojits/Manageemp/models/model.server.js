/*jslint anon:true, sloppy:true, nomen:true*/
/*global YUI, window, document*/
YUI.add('manageemp-model', function(Y, NAME) {
    'use strict';
    var CONFIG = Y.gbee.config;

    /**
     * The manageemp-model module.
     *
     * @module manageemp
     */

    /**
     * Constructor for the ManageempModel class.
     *
     * @class ManageempModel
     * @constructor
     */
    var request = require('request');
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            var url = CONFIG.endpoints.empPortal + 'employees.json?param=';
            request({
                url: url,
                method: 'GET',
                json: true

            }, function(err, res, body) {
                callback(null, body);
            });
        }

    };

}, '0.0.1', {
    requires: ['gbee-config']
});