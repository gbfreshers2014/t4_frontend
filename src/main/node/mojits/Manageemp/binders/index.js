/*jslint anon:true, sloppy:true, nomen:true*/
/*global YUI, window, document*/
YUI.add('manageemp-binder-index', function(Y, NAME) {
    'use strict';
    /**
     * The manageemp-binder-index module.
     *
     * @module manageemp-binder-index
     */

    /**
     * Constructor for the ManageempBinderIndex class.
     *
     * @class ManageempBinderIndex
     * @constructor
     */
    Y.namespace('mojito.binders')[NAME] = {

        /**
         * Binder initialization method, invoked after all binders on the page
         * have been constructed.
         */
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        /**
         * The binder method, invoked to allow the mojit to attach DOM event
         * handlers.
         *
         * @param node {Node} The DOM node to which this mojit is attached.
         */
        bind: function(node) {
            var me = this;
            this.node = node;
            //this.pagination(node);
        }
    };
}, '0.0.1', {
    requires: ['event-mouseenter', 'mojito-client']
});