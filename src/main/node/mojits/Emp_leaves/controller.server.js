/*jslint anon:true, sloppy:true, nomen:true*/
/*global YUI, window, document*/
YUI.add('emp_leaves', function(Y, NAME) {
    'use strict';
    /**
     * The emp_leaves module.
     *
     * @module emp_leaves
     */

    /**
     * Constructor for the Controller class.
     *
     * @class Controller
     * @constructor
     */
    Y.namespace('mojito.controllers')[NAME] = {

        /**
         * Method corresponding to the 'index' action.
         *
         * @param ac {Object} The ActionContext that provides access
         *        to the Mojito API.
         */
        index: function(ac) {
            //ac.params.getFromBody('startdate');
            //ac.models.get('model
            if (!(ac._adapter.req.user)) {
                ac.redirect('/');
            }
            var role;
            if (ac._adapter.req.user.role === 'admin') {
                role = 1;
            } else {
                role = 0;
            }
            var uuid = ac.params.getFromUrl('uuid');
            if(!uuid) {
                uuid = ac._adapter.req.user.uuid;
            }
            ac.models.get('model').getData(function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                ac.assets.addCss('./index.css');
                ac.done({
                    admin: role,
                    RoleName: ac._adapter.req.user.role,
                    uuid: uuid
                }, 'index');
            });
        },
        checkleaves: function(ac) {
            var startDate = ac.params.getFromUrl('startDate');
            var endDate = ac.params.getFromUrl('endDate');
            if (!(ac._adapter.req.user)) {
                ac.redirect('/');
            }
            var uuid = ac.params.getFromUrl('uuid');
            if(!uuid) {
                uuid = ac._adapter.req.user.uuid;
            }
            //console.log(uuid);
            ac.models.get('model').checkLeaves(uuid, startDate, endDate, function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                //console.log(data.leavesdetails);
                var len = data.leavesdetails.length;
                var arr = data.leavesdetails;
                data.leavesdetails = [];
                for(var i=0; i<len; i++) {
                    if(arr[i].date) {
                        var dateAr = arr[i].date.split('-');
                        if(dateAr.length === 3 && arr[i].status !== 'present') {
                            data.leavesdetails.push({
                                date: dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0],
                                status: arr[i].status
                            });
                            //data.leavesdetails[i].date = ;
                        }
                    }
                }
                //console.log(data.leavesdetails);
                ac.done({
                    RoleName: ac._adapter.req.user.role,

                    data: data.leavesdetails
                }, 'json');

            });
        }

    };

}, '0.0.1', {
    requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon', 'mojito-params-addon']
});