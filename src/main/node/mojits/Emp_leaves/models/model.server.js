/*jslint anon:true, sloppy:true, nomen:true*/
/*global YUI, window, document*/
YUI.add('emp_leaves-model', function(Y, NAME) {
    'use strict';
    var CONFIG = Y.gbee.config;
    /**
     * The emp_leaves-model module.
     *
     * @module emp_leaves
     */

    /**
     * Constructor for the Emp_leavesModel class.
     *
     * @class Emp_leavesModel
     * @constructor
     */
    var request = require('request');
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, {
                some: 'data'
            });
        },
        checkLeaves: function(uuid, startDate, endDate, callback) {
            var url = CONFIG.endpoints.empPortal + 'employees/' + uuid + '/leaves.json?start_date=' + startDate + '&end_date=' + endDate;
            request({
                url: url,
                //headers: {"Content-Type": "application/json"},
                method: 'GET',
                json: true

            }, function(err, res, body) {

                callback(null, body);

            });

        }

    };

}, '0.0.1', {
    requires: ['mojit-params-addon', 'gbee-config']
});