/*global YUI, window, document*/
YUI.add('addemp-model', function(Y, NAME) {
    "use strict";
    var CONFIG = Y.gbee.config;

    var request = require('request');
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },


        getData: function(callback) {

            callback(null, { some: 'data' });
        },
        pushData: function(data,callback){
            var url = CONFIG.endpoints.empPortal + "employees.json";
            request({
                url: url,
                headers: {"Content-Type": "application/json"},
                method: "POST",
                body:  JSON.stringify(data),
                json: true
            }, function(err,res, body){
                callback(err, body);
            });

        }


    };

}, '0.0.1', {requires: ['mojito-params-addon','mojito-client', 'gbee-config']});
