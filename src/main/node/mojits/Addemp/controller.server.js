/*global YUI, window, document*/
YUI.add('addemp', function(Y, NAME) {
    "use strict";
    Y.namespace('mojito.controllers')[NAME] = {
        index: function(ac) {
            if(!(ac._adapter.req.user) || (ac._adapter.req.user.role !== 'admin')) {
                ac.redirect('/');
                return;
            }
            ac.assets.addCss('./index.css');
            ac.models.get('model').getData(function(err, data) {
                if (err) {
                    ac.error(err);
                    return;
                }
                ac.done({
                    PanelName: 'Admin Panel',
                    RoleName: ac._adapter.req.user.role,
                    class1: 'selected',
                    status: 'Mojito is working.',
                    updatinguuid: ac._adapter.req.user.uuid,
                    data: data,
                    admin: true
                });
            });
        },
        addfn:function(ac)
        {
            var jsonObj = {};
            jsonObj.employeeDetails = {};

            jsonObj.loginCred = {
                role: ac.params.getFromBody('role'),
                password: ac.params.getFromBody('password')
            };

            jsonObj.employeeDetails.personalDetails = {
                firstName: ac.params.getFromBody('firstName'),
                lastName: ac.params.getFromBody('lastName'),
                email: ac.params.getFromBody('email'),
                employeeId: ac.params.getFromBody('empId'),
                designation: ac.params.getFromBody('designation'),
                employmentStatus: ac.params.getFromBody('empStatus'),
                bloodGroup: ac.params.getFromBody('bloodGroup'),
                dob: ac.params.getFromBody('dob'),
                doj: ac.params.getFromBody('doj')
            };
            var personalDetailsValidator = Y.gbee.personalDetailsValidator;
            jsonObj.employeeDetails.communicationDetails = [];
            jsonObj.employeeDetails.addressDetails = [];
            jsonObj.employeeDetails.familyDetails = [];
            if(ac._adapter.req.user) {
                jsonObj.updatingUuid = ac._adapter.req.user.uuid;
            }
            else {
                ac.done({message : "You are not an autenticated user"}, 'upload_status');
                return;
            }

            if(!(ac._adapter.req.user)) {
                ac.done({message: "You are not an authenticated user"}, 'upload_status');
                return;
            }
            ac.models.get('model').pushData(jsonObj,function(err, data) {
                var message, uuid, admin;
                if (err) {
                    message = 'Internal Server Error';
                }

                admin = (ac._adapter.req.user.role === 'admin');
                if(data && data.status && parseInt(data.status.code) === 0) {
                    message = "New employee added successfully";
                    uuid = data.uuid;
                }
                else {
                    message = 'There was an error in adding this employee. Possible reason might be the email entered is not unique';
                }
                ac.done({
                    message:message,
                    uuid: uuid,
                    admin: admin
                }, 'upload_status');
            });
        }

    };

}, '0.0.1', {requires: ['mojito', 'mojito-assets-addon', 'mojito-models-addon', 'mojito-params-addon', 'gbee-validator']});
