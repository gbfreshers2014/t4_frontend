/*global YUI, window, document*/
YUI.add('addemp-binder-index', function(Y, NAME) {
    "use strict";
    Y.namespace('mojito.binders')[NAME] = {
        init: function(mojitProxy) {
            this.mojitProxy = mojitProxy;
        },

        bind: function(node) {
            var me = this;
            this.node = node;
            this.validation.addValidationEvent();
            this.bindSubmit(node, me);
            var validation = this.validation;

            var dobshowNode = Y.one('#dobshow'),
                dobNode = Y.one('#dob');
            dobshowNode.on('change', function(e) {
                var dobshow = e.target.get('value');
                if (validation.validateDobDMY()) {
                    var dobArr = dobshow.split('-');
                    dobNode.set('value', (dobArr[2] + '-' + dobArr[1] + '-' + dobArr[0]));
                }
            });
            var currdate = new Date();

            var calendar = new Y.Calendar({
                contentBox: '#mycalendarDob',
                width: '200px',
                showPrevMonth: true,
                showNextMonth: true,
                date: new Date(),
                //minimumDate: new Date(currdate.getFullYear(), '00', '01'),
                maximumDate: new Date()
            }).render();

            calendar.plug(Y.Plugin.Drag);
            calendar.plug(Y.Plugin.Calendar.JumpNav, {
                yearStart: 1900,
                yearEnd: currdate.getFullYear(),
                closeAfterGo: false
            });

            var calendarDoj = new Y.Calendar({
                contentBox: '#mycalendarDoj',
                width: '200px',
                showPrevMonth: true,
                showNextMonth: true,
                date: new Date(),
                //minimumDate: new Date(currdate.getFullYear(), '00', '01'),
                maximumDate: new Date()
            }).render();

            calendarDoj.plug(Y.Plugin.Drag);
            calendarDoj.plug(Y.Plugin.Calendar.JumpNav, {
                yearStart: 1900,
                yearEnd: currdate.getFullYear(),
                closeAfterGo: false
            });

            var dtdate = Y.DataType.Date;

            // Listen to calendar's selectionChange event.
            calendar.on('selectionChange', function(ev) {

                // Get the date from the list of selected
                // dates returned with the event (since only
                // single selection is enabled by default,
                // we expect there to be only one date)
                var newDate = ev.newSelection[0];

                // Format the date and output it to a DOM
                // element.
                //Y.one("#datevalue").setHTML(dtdate.format(newDate));
                Y.one('#dob').set('value', dtdate.format(newDate));
                Y.one('#dobshow').set('value', dtdate.format(newDate, {
                    format: '%d-%m-%Y'
                }));
            });

            calendarDoj.on('selectionChange', function(ev) {

                // Get the date from the list of selected
                // dates returned with the event (since only
                // single selection is enabled by default,
                // we expect there to be only one date)
                var newDate = ev.newSelection[0];

                // Format the date and output it to a DOM
                // element.
                //Y.one("#datevalue").setHTML(dtdate.format(newDate));
                Y.one('#doj').set('value', dtdate.format(newDate));
                Y.one('#dojshow').set('value', dtdate.format(newDate, {
                    format: '%d-%m-%Y'
                }));
            });
        },

        bindSubmit: function(node, me) {
            var submitButton = Y.one('#submit_button');
            submitButton.on('click', function() {
                var dob = Y.one('#dob').get('value');
                var doj = Y.one('#doj').get('value');
                //console.log(dob);
                if(!dob || !doj) {
                    window.alert('Please enter Date of Birth and Date of Joining');
                    return;
                }
                /*var employeeId = Y.one('#empId').get('value');
                if(!employeeId) {
                    window.alert('Please enter Employee Id');
                    return;
                }*/
                if(me.validation.validateAll()) {
                    Y.one('#addempform').submit();
                }
                else
                {
                    window.alert('Please correct the fields marked as Red');
                }
            });
        },

        validation: {
            addValidationEvent: function() {
                var personalDetailsValidator = Y.gbee.personalDetailsValidator;

                var firstNameNode = Y.one('#firstName'),
                    lastNameNode = Y.one('#lastName'),
                    emailIdNode = Y.one('#emailId'),
                    bloodGroupNode = Y.one('#bloodGroup'),
                    passwordNode = Y.one('#password'),
                    employeeIdNode = Y.one('#empId'),
                    designationNode = Y.one('#designation');

                var dd;

                firstNameNode.on('change', function(e) {
                    var firstName = e.target.get('value');

                    if (!(personalDetailsValidator.nameValidator(firstName))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
                lastNameNode.on('change', function(e) {
                    var lastName = e.target.get('value');

                    if (!(personalDetailsValidator.nameValidator(lastName))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
                emailIdNode.on('change', function(e) {
                    var emailId = e.target.get('value');

                    if (!(personalDetailsValidator.emailValidator(emailId))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
                bloodGroupNode.on('change', function(e) {
                    var bloodGroup = e.target.get('value');

                    if (!(personalDetailsValidator.bloodGroupValidator(bloodGroup))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
                employeeIdNode.on('change', function(e) {
                    var employeeId = e.target.get('value');

                    if (!(personalDetailsValidator.empIdValidator(employeeId))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
                designationNode.on('change', function(e) {
                    var designation = e.target.get('value');

                    if (!(personalDetailsValidator.designationValidator(designation))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
                passwordNode.on('change', function(e) {
                    var password = e.target.get('value');

                    if (!(personalDetailsValidator.passwordValidator(password))) {
                        e.target.addClass('invalidField');
                    } else {
                        e.target.removeClass('invalidField');
                    }
                });
            },

            validateFirstName: function () {
                if(Y.gbee.personalDetailsValidator.nameValidator(Y.one('#firstName').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#firstName').addClass('invalidField');
                    return false;
                }
            },

            validateLastName: function () {
                if(Y.gbee.personalDetailsValidator.nameValidator(Y.one('#lastName').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#lastName').addClass('invalidField');
                    return false;
                }
            },

            validateEmail: function () {
                if(Y.gbee.personalDetailsValidator.emailValidator(Y.one('#emailId').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#emailId').addClass('invalidField');
                    return false;
                }
            },

            validateBloodGroup: function () {
                if(Y.gbee.personalDetailsValidator.bloodGroupValidator(Y.one('#bloodGroup').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#bloodGroup').addClass('invalidField');
                    return false;
                }
            },

            validateEmployeeId: function () {
                if(Y.gbee.personalDetailsValidator.empIdValidator(Y.one('#empId').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#empId').addClass('invalidField');
                    return false;
                }
            },
            validateDesignation: function () {
                if(Y.gbee.personalDetailsValidator.designationValidator(Y.one('#designation').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#designation').addClass('invalidField');
                    return false;
                }
            },

            validatePassword: function () {
                if(Y.gbee.personalDetailsValidator.passwordValidator(Y.one('#password').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#password').addClass('invalidField');
                    return false;
                }
            },

            validateDob: function () {
                if(Y.gbee.personalDetailsValidator.dobValidator(Y.one('#dob').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#dob').addClass('invalidField');
                    return false;
                }
            },
            validateDobDMY: function () {
                if(Y.gbee.personalDetailsValidator.dobValidatorDMY(Y.one('#dobshow').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#dobshow').addClass('invalidField');
                    return false;
                }
            },

            validateDoj: function () {
                if(Y.gbee.personalDetailsValidator.dojValidator(Y.one('#doj').get('value'))) {
                    return true;
                }
                else {
                    Y.one('#doj').addClass('invalidField');
                    return false;
                }
            },

            validateAll: function () {
                return (this.validateFirstName() &&
                    this.validateLastName() &&
                    this.validateEmail() &&
                    this.validateDesignation() &&
                    this.validateBloodGroup() &&
                    this.validateEmployeeId() &&
                    this.validatePassword());// &&
                    //this.validateDob() &&
                    //this.validateDoj());
            }
        }

    };
}, '0.0.1', {
    requires: ['event-mouseenter', 'mojito-client', 'event-click', 'calendar', 'gallery-calendar-jumpnav', 'gbee-validator']
});