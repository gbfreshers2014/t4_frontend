/*jslint anon:true, sloppy:true, nomen:true*/
/*global YUI, window, document*/
YUI.add('admin-model', function(Y, NAME) {
    'use strict';
    var CONFIG = Y.gbee.config;
    /**
     * The admin-model module.
     *
     * @module admin
     */

    /**
     * Constructor for the AdminModel class.
     *
     * @class AdminModel
     * @constructor
     */
    var request = require('request');
    Y.namespace('mojito.models')[NAME] = {

        init: function(config) {
            this.config = config;
        },

        /**
         * Method that will be invoked by the mojit controller to obtain data.
         *
         * @param callback {function(err,data)} The callback function to call when the
         *        data has been retrieved.
         */
        getData: function(callback) {
            callback(null, {
                some: 'data'
            });
        },

        getmissingdates: function(callback) {
            var url = CONFIG.endpoints.empPortal + 'notification.json';
            request({
                url: url,
                method: 'GET'

            }, function(err, res, body) {


                callback(err, body);

            });


        }

    };

}, '0.0.1', {
    requires: ['mojito-rest-lib', 'mojito-params-addon', 'gbee-config']
});