/*global YUI, window, document*/
YUI.add('admin', function(Y, NAME) {
    "use strict";
    Y.namespace('mojito.controllers')[NAME] = {

        index: function(ac) {
            if (ac._adapter.req.user.role !== "admin") {
                ac.redirect('/profile'); //insert proper redirect command here
                return;
            }
            ac.models.get('model').getmissingdates(function(err, res) {
                if (err) {
                    ac.error(err);
                    return;
                }
                res = JSON.parse(res);
                var missing, resArr = [];
                if (res.status && (parseInt(res.status.code) === 0)) {
                    var len = res.ald.length,
                        date_show,
                        date_el;
                    for (var i=0; i<len; i++) {
                        date_el = res.ald[i].split('-');
                        date_show = date_el[2] + '-' + date_el[1] + '-' + date_el[0];
                        resArr.push({
                            date: res.ald[i],
                            date_show: date_show
                        });
                    }
                    missing = res.ald;
                }
                ac.done({
                    PanelName: 'Admin Panel',
                    RoleName: ac._adapter.req.user.role,
                    admin: 'true',
                    class1: 'selected',
                    status: 'Admin Page is working.',
                    missing: resArr
                });
            });

        },
        missingdata: function(ac) {
            ac.models.get('model').getmissingdates(function(err, res) {
                if (err) {
                    ac.done();
                    return;
                }
                if (res.status && parseInt(res.status.code) === 0) {
                    var len = res.ald.length,
                        resArr = [],
                        date_show,
                        date_el;
                    for (var i=0; i<len; i++) {
                        date_el = res.ald[i].split('-');
                        resArr.push({
                            date: res.ald[i],
                            date_show: null
                        });
                    }
                    ac.done({
                        missing: res.ald
                    });
                } else {
                    ac.done();
                }
            });
        }

    };

}, '0.0.1', {
    requires: ['mojito', 'mojito-assets-addon', 'mojito-session-addon', 'mojito-params-addon', 'admin-model', 'mojito-http-addon', 'mojito-models-addon']
});