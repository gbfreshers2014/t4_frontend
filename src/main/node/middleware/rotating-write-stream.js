/**
 * Copyright 2013 Gwynnie Bee Inc.
 */

"use strict";

var fs = require("fs");
var EE = require('events').EventEmitter;
var util = require('util');


// Encapsulate a normal WriteStream..
// TODO: All events must also be re-emitted?
function RotatingWriteStream(path, options) {
    this._path = path;
    this._options = options;
    this._stream = fs.createWriteStream(path, options);
    var self = this;
    this._stream.on("error", function (e) {
        self.emit("error", e);
    });
    process.on("SIGUSR2", function() {
        self.reopen();
    });
}

util.inherits(RotatingWriteStream, EE);

RotatingWriteStream.prototype.reopen = function () {
    if (this._stream) {
        this._stream.end();
    }
    var self = this;
    this._stream = fs.createWriteStream(this._path, this._options);
    this._stream.on("error", function (e) {
        self.emit("error", e);
    });
};

RotatingWriteStream.prototype.write = function (chunk, encoding, callback) {
    return this._stream.write(chunk, encoding, callback);
};

RotatingWriteStream.prototype.end = function (chunk, encoding, callback) {
    return this._stream.end(chunk, encoding, callback);
};


module.exports = RotatingWriteStream;