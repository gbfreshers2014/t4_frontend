/**
 * Copyright 2012 GwynnieBee Inc.
 */

/* jslint node: true */

"use strict";

/**
 * Module to log access records to the server.
 */

var express = require("mojito/node_modules/express"),
    RotatingWriteStream = require("./rotating-write-stream"),
    config = require("config");


// :date outputs as "Tue, 19 Mar 2013 11:56:51 GMT"
// :timestamp outputs as "2013-03-19T11:56:51.693Z"
express.logger.token("timestamp", function () {
    return new Date().toISOString();
});

// override the default response-time token to add "ms"
express.logger.token("response-time", function (req) {
    return new Date() - req._startTime + "ms";
});

var stream = new RotatingWriteStream(config.logs.access, {
    flags: "a",
    encoding: "utf-8"
});

// if you fail to write to file, log once and then just stay silent..
stream.on("error", function (e) {
    if (e.code) {
        console.log("Error writing access logs, Requests might not get logged. Error:\n", e);
    }
});

var options = {
    format: '[:timestamp] :remote-addr ":method :url HTTP/:http-version" :status ":referrer" ":user-agent" :response-time',
    immediate: false,   // Log after waiting for response (to calculate response time)
    buffer: 1000,       // in milliseconds
    stream: stream
};

module.exports = express.logger(options);