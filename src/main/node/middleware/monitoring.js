/**
 * Copyright 2013 GwynnieBee Inc.
 */

"use strict";

var StatsD = require("node-statsd").StatsD,
    config = require("config").monitoring;

var client = new StatsD({
    host: config.host,
    port: config.port,
    prefix: config.prefix
});

client.socket.on("error", function (error) {
    console.error("Error in Monitoring socket:", error);
});

function monitor(req, res, next) {
    var startTime = Date.now();
    var end = res.end;

    res.end = function () {
        end.apply(res, arguments);

        var duration = Date.now() - startTime; // in milliseconds

        // total data
        client.increment("requests");
        client.timing("response_time", duration);

        // aggregated by http status code
        client.increment("response_code." + res.statusCode);
        client.timing("response_time." + res.statusCode, duration);
    };
    next();
}

// Export the monitor function.
module.exports = monitor;