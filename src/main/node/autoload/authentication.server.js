"use strict";

var passport = require('passport'),
  http = require('http'),
  LocalStrategy = require('passport-local').Strategy,
  CONFIG = require("config"),
  request = require('request');

// Passport session setup.
//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(id, done) {
  //findById(id, function (err, user) {
    done(null, id);
  //});
});

passport.use(new LocalStrategy(
  function(username, password, done) {
    // asynchronous verification, for effect...
      //console.log(username + " " + password);
    var jsonObj = JSON.stringify({
      emailid : username,
      password : password
    });
    var postheaders = {
      'Content-Type' : 'application/json',
      'Content-Length' : Buffer.byteLength(jsonObj, 'utf8')
  };
  //console.log(CONFIG.endpoints.empPortal + 'authenticate.json');
  var optionspost = {
      host : CONFIG.endpoints.empPortal,
      port : 8080,
      path : 'authenticate.json',
      method : 'POST',
      headers : postheaders
  };


    process.nextTick(function () {
      var resp_data = {};
      var url = CONFIG.endpoints.empPortal + 'authenticate.json';
      //console.log(jsonObj);
      request({
              url: url,
              headers: {"Content-Type": "application/json"},
              method: "POST",
              body:  jsonObj,
              json: true
            }, function(err,res, body){
              if(err) {
                return done("Internal Server Error");
              }
              if(body && body.status) {
                if(body.status.code === 0) {
                  delete body.status;
                  return done(null, body);
                }
                else {
                  done(null, null);
                }
              }
              else {
                return done("Internal Server Error");
              }
              //done(err, body);
      });
    });
  }
));

passport.ensureAuthenticated = function (req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/');
};

passport.ensureAdmin = function (req, res, next) {
  if(req.isAuthenticated() && req.user && (req.user.role === "admin")) {
    return next();
  }
  req.logout();
  res.redirect('/');
};

exports.passport = passport;
//exports.ensureAuthenticated = ensureAuthenticated;
