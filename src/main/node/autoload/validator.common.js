/*global YUI, window, document*/
YUI.add('gbee-validator',function (Y, NAME){
    'use strict';
    var personalDetailsValidator =  {
        /*var email,
            firstName,
            lastName;*/
        //email validator     
        emailValidator : function emailValidator(email) {
            // First check if any value was actually set
            if (email.length === 0) {
                return false;
            }
            // Now validate the email format using Regex
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]                         {1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        },

        //function for validating first name of a person
        nameValidator : function nameValidator(firstName) {
            var reg_name = /^[A-Za-z \s \.]{1,35}$/;
            return reg_name.test(firstName);
        },

        //function for validating blood group
        bloodGroupValidator : function bloodGroupValidator(bloodGroup) {
            var reg_blood = /^(?:(A|B|AB|O)[+-])?$/;
            return reg_blood.test(bloodGroup);
        },

        //funcion for full name (mother, father) validator
        fullNameValidator : function fullNameValidator(fullName) {
            var reg_fullname = /^([A-z\'\.-ᶜ]*(\s))*[A-z\'\.-ᶜ]*$/;
            return reg_fullname.test(fullName);
        },
        /*email = json.email;
        if (!emailValidator(email)) {
            err = 1;
            Y.one('#email').addClass("missing_class");
        }

        firstName = json.firstName;
        if (!nameValidator(firstName)) {
            err = 1;
            Y.one('#firstName').addClass("missing_class");
        }

        lastName = json.lastName;
        if (!namevalidator(lastName)) {
            err = 1;
            Y.one('#lastName').addClass("missing_class");
        }

        bloodGroup = json.bloodGroup;
        if (!bloodGroupValidator(bloodGroup)) {
            err = 1;
            Y.one('#bloodGroup').addClass("missing_class");
        }

        fatherName = json.fatherName;
        if (!fullNameValidator(fatherName)) {
            err = 1;
            Y.one('#fatherName').addClass("missing_class");
        }

        motherName = json.motherName;
        if (!fullNameValidator(motherName)) {
            err = 1;
            Y.one('#motherName').addClass("missing_class");
        }*/
        phoneNoValidator:function phoneNoValidator(phoneNo){
            var reg_phone=  /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
            return reg_phone.test(phoneNo);
        },
    
        /*phoneNo = json.phoneNo;
        if (!(phoneNo.test(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/))) {
                err = 1;
                Y.one('#phoneNo').addClass("missing_class");
        }*/
        skypeIdValidator:function skypeIdValidator(skypeId){
            var reg_skype=/^[a-z][a-z0-9\.,\-_]{5,31}$/i;
            return reg_skype.test(skypeId);
        },
            /*skypeId = json.skypeId;
            if (!(skypeId.test(/^[a-z][a-z0-9\.,\-_]{5,31}$/i))) {
                    err = 1;
                    Y.one('#skypeId').addClass("missing_class");
                }*/
        empIdValidator:function empIdValidator(empId){
            var reg_empId=/^(IN|US)[0-9]{1,3}$/;
            return reg_empId.test(empId);
        },
        uuidValidator:function uuidValidator(uuid){
            var reg_uuid=/^[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}$/;
            return reg_uuid.test(uuid);
            
        },
        dobValidator:function dobValidator(dob){
            var reg_dob=/^((1[0-9]{3})|(2[0][01][0-3]))-[1-12]-[1-31]$/;
            return reg_dob.test(dob);
        },
        dobValidatorDMY:function dobValidator(dob){
            var reg_dob=/^(1-31)-(1-12)-((1[0-9]{3})|(2[0][01][0-3]))$/;
            return reg_dob.test(dob);
        },
        dojValidator:function dojValidator(doj){
            var reg_doj=/^((1[0-9]{3})|(2[0][01][0-3]))-[1-12]-[1-31]$/;
            return reg_doj.test(doj);
        },
        passwordValidator:function passwordValidator(password){
            var reg_pass=/^\w+$/;
            return reg_pass.test(password);
        },
        designationValidator:function designationValidator(designation){
            var reg_desig=/^[A-Za-z\.]*$/;
            return reg_desig.test(designation);
        }
                
    };

    Y.namespace('gbee').personalDetailsValidator = personalDetailsValidator;
}, '0.0.1', {requires: []});