/**
 * Copyright 2012 GwynnieBee Inc.
 */

/*jslint anon:true, nomen:true*/
/*global YUI, require*/

YUI.add("gbee-config", function (Y, NAME) {

    "use strict";

    Y.namespace("gbee").config = require("config");

}, "0.0.1", {requires: []});
