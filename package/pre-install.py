#! /usr/bin/env python
#
# Does the following:
# - Runs mojito tests
# - Stops existing service if any
#

import os
import sys

mojito_app_dir = "/home/gb/share/mojito/employee-portal-t4-app"


def main():
    """Main function"""

    print "Running pre-install script..."

    # Stop service if running.
    if os.path.islink("/service/employee-portal-t4-app"):
        os.chdir("/service/employee-portal-t4-app")

        # Remove the symlink
        try:
            print "Unlinking /service/employee-portal-t4-app"
            os.unlink("/service/employee-portal-t4-app")
        except Exception as ex:
            print "Exception in unlinking /service/employee-portal-t4-app", ex

        print "Stopping existing service..."
        command = "svc -dx ."
        result = os.system(command)
        if result != 0:
            print "Failed to stop existing service - employee-portal-t4-app"
            sys.exit(1)
        command = "svc -dx log"
        result = os.system(command)
        if result != 0:
            print "Failed to stop existing service - employee-portal-t4-app"
            sys.exit(1)
    else:
        print "No existing service found for employee-portal-t4-app"

if __name__ == "__main__":
    main()
