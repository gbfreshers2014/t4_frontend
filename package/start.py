#! /usr/bin/env python
#
# Runs mojito.
#

import os
import sys

empportal_app_properties = "/home/gb/conf/employee-portal-t4-app/configuration.properties"

def parse_config_file(filename):
    """Parse configuration file and return a hash"""
    result = dict()
    file = open(filename, "rb")
    while True:
        line = file.readline()
        if not line:
            break
        line = line.rstrip("\r\n")
        if line != "":
            [key, value] = line.split("=")
            result[key] = value
    return result


def main():
    """Main function"""
    # Parse config.
    config = parse_config_file(empportal_app_properties)

    # Setting deployment mode
    if "deploy.mode" in config:
        os.environ["NODE_ENV"] = config["deploy.mode"]

    print "Starting application..."
    parameters = ["employee-portal-t4-app"]

    # See if we need to turn on debugging.
    if config.has_key("server.deploy.debug.port"):
        parameters.append("--debug=" + config["server.deploy.debug.port"])

    parameters.append("app.js")
    os.execv("/usr/bin/node", parameters)
    print "Mojito start failed. Exiting"
    sys.exit(1)

if __name__ == "__main__":
    main()
